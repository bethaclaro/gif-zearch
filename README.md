# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

## Notes

### Time consumed
Start: 8:00AM, June 25 UTC+8\
End: 10:45PM, June 25 UTC+8\
** With naps and breaks in between.

### Third party libraries used
[Masonic](https://github.com/jaredLunde/masonic) - for the infinite scrolling Grid component\
[Styled-components](https://styled-components.com) - for additional styling\
[react-hook-modal](https://www.npmjs.com/package/react-hook-modal) - for the modal on click of a gif item


### What's missing
[ ] Better test coverage\
[ ] End-to-end testing