import React from 'react';
import { render, screen, cleanup } from '@testing-library/react';
// import App from '../App';
import MasonSearch from '../containers/MasonSearch';

afterEach(cleanup);

test('renders search box', () => {
  render(<MasonSearch />);
  const inputElement = screen.getByPlaceholderText('Input search query...');
  expect(inputElement).toBeInTheDocument();
});
