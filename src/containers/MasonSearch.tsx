/* eslint-disable jsx-a11y/media-has-caption */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable @typescript-eslint/naming-convention */
import {
  useContainerPosition, useInfiniteLoader, useMasonry, usePositioner, useScroller,
} from 'masonic';
import React, {
  useContext, useEffect, useRef, useState,
} from 'react';
import { IGif } from '@giphy/js-types';
import { SearchContext, SearchContextManager } from '@giphy/react-components';
import { useWindowSize } from '@react-hook/window-size';
import { Modal, useModal } from 'react-hook-modal';
import giphyFetch, { apiKey } from '../common/api';
import {
  Card, PaddedSearchBar, PaddedWrapper, GifDisplayItem, GifDisplayWrapper,
} from './styles';
import 'react-hook-modal/dist/index.css';
import { MasonryItemProps, ModalContentProps, SearchComponentProps } from './types';

const MasonryItem = ({ data, setSelectedGif }: MasonryItemProps) => {
  const { images: { fixed_width_small_still } } = data;
  return (
    <Card onClick={() => setSelectedGif(data)}>
      <img
        src={fixed_width_small_still.url}
        width={100}
        height={100}
        alt="still gif"
      />
    </Card>
  );
};

const SearchComponent = (props: SearchComponentProps) => {
  const { setSearchKey } = props;
  const { searchKey } = useContext(SearchContext);

  useEffect(() => {
    setSearchKey(searchKey);
  }, [searchKey]);

  return (
    <PaddedSearchBar placeholder="Input search query..." />
  );
};

const ModalContent = ({ selectedGif }: ModalContentProps) => {
  const {
    images: {
      downsized, preview_gif, looping,
    },
    title,
  } = selectedGif;

  return (
    <GifDisplayWrapper>
      <GifDisplayItem>
        <span>Preview</span>
        <img src={preview_gif.url} alt={title} width={preview_gif.width} />
      </GifDisplayItem>
      <GifDisplayItem>
        <span>Downsized</span>
        <img src={downsized.url} alt={title} width={120} />
      </GifDisplayItem>
      <GifDisplayItem>
        <span>Looping</span>
        <video height="250" controls>
          <source src={looping.mp4} type="video/mp4" />
        </video>
      </GifDisplayItem>
    </GifDisplayWrapper>
  );
};

const MasonSearch: React.FC = () => {
  const [searchKey, setSearchKey] = useState<string>('');
  const [gifStills, setGifStills] = useState<IGif[]>([]);
  const [currentIndex, setCurrentIndex] = useState(0);

  const containerRef = useRef(null);
  const [windowWidth, height] = useWindowSize();
  const { offset, width } = useContainerPosition(containerRef, [
    windowWidth, height,
  ]);
  const { scrollTop, isScrolling } = useScroller(offset);
  const positioner = usePositioner({ width, columnGutter: 4, rowGutter: 4 }, [gifStills]);

  const { setComponentToRender } = useModal();

  const loadMore = useInfiniteLoader(
    async (startIndex) => {
      const nextItems = await giphyFetch
        .search(searchKey, { offset: startIndex, limit: 10 });
      setGifStills((current) => [...current, ...nextItems.data]);
      setCurrentIndex(startIndex);
    },
    {
      isItemLoaded: (index, items) => Boolean(items[index]) === true,
      minimumBatchSize: 10,
      threshold: 10,
    },
  );

  const getGifStills = async () => {
    const temp = await giphyFetch.search(searchKey, { offset: currentIndex, limit: 10 });
    setGifStills(temp.data);
  };

  useEffect(() => {
    getGifStills();
  }, [searchKey]);

  const fnSelectGif = (data: IGif) => {
    setComponentToRender(<ModalContent selectedGif={data} />, {
      title: 'GIF Details',
    });
  };

  return (
    <SearchContextManager apiKey={apiKey}>
      <PaddedWrapper>
        <SearchComponent setSearchKey={setSearchKey} />
        {useMasonry({
          positioner,
          scrollTop,
          isScrolling,
          height,
          containerRef,
          items: gifStills,
          render: (params) => (
            <MasonryItem
              {...params}
              setSelectedGif={fnSelectGif}
            />
          ),
          onRender: loadMore,
        })}
      </PaddedWrapper>
      <Modal />
    </SearchContextManager>
  );
};

export default MasonSearch;
