import { SearchBar } from '@giphy/react-components';
import styled from 'styled-components';

export const Card = styled.div`
  display: flex;
  width: 110px;
  height: 100px;
  cursor: pointer;
`;

export const PaddedWrapper = styled.div`
  margin: 30px auto;
  width: 60vw;
`;

export const PaddedSearchBar = styled(SearchBar)`
margin-bottom: 20px;
`;

export const GifDisplayWrapper = styled.div`
  margin: 5px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const GifDisplayItem = styled.div`
  margin-top: 5px;
  margin-bottom: 5px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
