import { IGif } from '@giphy/js-types';

export type MasonryItemProps = {
  data: any,
  setSelectedGif: Function,
};

export type SearchComponentProps = {
  setSearchKey: Function,
};

export type ModalContentProps = {
  selectedGif: IGif,
};
