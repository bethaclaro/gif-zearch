import React from 'react';
import { ModalDataContextProvider } from 'react-hook-modal';
import MasonSearch from './containers/MasonSearch';

const App: React.FC = () => (
  <ModalDataContextProvider>
    <MasonSearch />
  </ModalDataContextProvider>
);

export default App;
